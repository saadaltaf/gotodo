package controllers

import (
	"fmt"
	"gitlab/saadaltaf/gotodo/api/middlewares"
	"gitlab/saadaltaf/gotodo/api/models"
	"gitlab/saadaltaf/gotodo/api/responses"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

type App struct {
	DB     *gorm.DB
	Router *mux.Router
}

// Initialize connect to the database and wire up routes
func (a *App) Initialize(DbHost, DbPort, DbUser, DbName, DbPassword string) {
	var err error
	DBURI := fmt.Sprintf("host=%s port=%s user=%s dbname=%s sslmode=disable password=%s", DbHost, DbPort, DbUser, DbName, DbPassword)

	//TODO: Important not to log
	//fmt.Println("DBURI: ", DBURI)

	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second,   // Slow SQL threshold
			LogLevel:                  logger.Silent, // Log level
			IgnoreRecordNotFoundError: true,          // Ignore ErrRecordNotFound error for logger
			Colorful:                  true,          // Disable color
		},
	)

	// *Had to modify it from the tutorial: https://gorm.io/docs/connecting_to_the_database.html
	a.DB, err = gorm.Open(postgres.Open(DBURI), &gorm.Config{Logger: newLogger})
	if err != nil {
		fmt.Printf("\n Cannot connect to database %s", DbName)
		log.Fatal("This is the error:", err)
	} else {
		fmt.Printf("We are connected to the database %s", DbName)
	}

	a.DB.AutoMigrate(&models.User{}, &models.Task{}) //database migration

	a.Router = mux.NewRouter().StrictSlash(true)
	a.initializeRoutes()
}

func (a *App) initializeRoutes() {
	a.Router.Use(middlewares.SetContentTypeMiddleware) // setting content-type to json
	//? Can we set the jwt authentication as a middleware func for gorilla mux?
	//? Apparently not, jwt is best generated when signing in, shouldn't be used for session authentication

	//Routes

	//home route
	a.Router.HandleFunc("/", home).Methods("GET")

	//user routes
	a.Router.HandleFunc("/register", a.UserSignUp).Methods("POST")
	a.Router.HandleFunc("/login", a.Login).Methods("POST")
	a.Router.HandleFunc("/users/{id}", middlewares.SetMiddlewareAuth(a.GetUser)).Methods("GET")
	a.Router.HandleFunc("/users/{id}", middlewares.SetMiddlewareAuth(a.DeleteUser)).Methods("DELETE")

	//TODO: Evaluate routes. Since tasks cant exist without user, better to prepend with user/{id}/
	//TODO: e.g /user/16/tasks/12 for deleting task id 12 for user id 16, but maybe using JWTs prevents having to use this?
	//TODO: Done using https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/

	//tasks routes
	a.Router.HandleFunc("/users/{uid}/tasks", middlewares.SetMiddlewareAuth(a.CreateTask)).Methods("POST")
	a.Router.HandleFunc("/users/{uid}/tasks", middlewares.SetMiddlewareAuth(a.GetTasks)).Methods("GET")
	a.Router.HandleFunc("/users/{uid}/tasks/{id}", middlewares.SetMiddlewareAuth(a.GetTask)).Methods("GET")
	a.Router.HandleFunc("/users/{uid}/tasks/{id}", middlewares.SetMiddlewareAuth(a.UpdateTask)).Methods("PUT")
	a.Router.HandleFunc("/users/{uid}/tasks/{id}", middlewares.SetMiddlewareAuth(a.DeleteTask)).Methods("DELETE")

}

func (a *App) RunServer() {
	log.Printf("\nServer starting on port 5000")

	//! Running Server in a go routine when running  (otherwise tests get stuck)
	//! Better to run directly when testing through curl or Postman
	// go func() {
	// 	log.Fatal(http.ListenAndServe(":5000", a.Router))
	// }()

	log.Fatal(http.ListenAndServe(":5000", a.Router))

}

func home(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "Welcome To GO TODOs")
}
