package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitlab/saadaltaf/gotodo/api/auth"
	"gitlab/saadaltaf/gotodo/api/models"
	"gitlab/saadaltaf/gotodo/api/responses"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func (a *App) CreateTask(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uid, err := strconv.ParseUint(vars["uid"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return

	}

	//Set up Task model
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	task := models.Task{}

	err = json.Unmarshal(body, &task)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	task.Prepare()

	err = task.Validate("create")
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	//check if we an authorized token id?
	//TODO: Do we need to do this if the authentication middldleware is working?
	auth_uid, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	//Check if the uid for the token matches the uid for the request
	if uint32(uid) != auth_uid {
		responses.ERROR(w, http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	//Important to assign foreign key
	task.OwnerID = auth_uid

	taskCreated, err := task.SaveTask(a.DB)
	if err != nil {
		//formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	w.Header().Set("Location", fmt.Sprintf("%s%s/%d", r.Host, r.URL.Path, taskCreated.ID))
	responses.JSON(w, http.StatusCreated, taskCreated)
}

func (a *App) GetTasks(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	uid, err := strconv.ParseUint(vars["uid"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return

	}

	//check if we an authorized token id?
	//TODO: Do we need to do this if the authentication middldleware is working?
	auth_uid, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	//Check if the uid for the token matches the uid for the request
	if uint32(uid) != auth_uid {
		responses.ERROR(w, http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	var result map[string]interface{}
	err = json.Unmarshal(body, &result)

	fmt.Println("GetTasks for", uint32(uid))

	tasks, err := models.GetAllTasksForUser(a.DB, uint32(uid))
	if err != nil {
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}

	responses.JSON(w, http.StatusOK, tasks)
}

func (a *App) GetTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	uid, err := strconv.ParseUint(vars["uid"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return

	}

	tid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	//check if we an authorized token id?
	//TODO: Do we need to do this if the authentication middldleware is working?
	auth_uid, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	//Check if the uid for the token matches the uid for the request
	if uint32(uid) != auth_uid {
		responses.ERROR(w, http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	task := models.Task{}

	taskReceived, err := task.FindTaskByID(a.DB, tid)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}
	responses.JSON(w, http.StatusOK, taskReceived)
}

func (a *App) UpdateTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	uid, err := strconv.ParseUint(vars["uid"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return

	}

	tid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	//check if we an authorized token id?
	//TODO: Do we need to do this if the authentication middldleware is working?
	auth_uid, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	//Check if the uid for the token matches the uid for the request
	if uint32(uid) != auth_uid {
		responses.ERROR(w, http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	fmt.Printf("UpdateTask: for uid %v and tid %v\n\n", auth_uid, tid)
	// Check if the task exist
	task := models.Task{}

	taskReceived, err := task.FindTaskByID(a.DB, tid)
	if err != nil {
		fmt.Println(("UpdateTask:Task not found!!"))
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	// If a user attempt to update a task not belonging to him
	if auth_uid != taskReceived.OwnerID {
		fmt.Println(("UpdateTask: Owner id mismatched"))
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	// Read the data tasked
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(("UpdateTask: Body parsing failed"))
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	// Start processing the request data
	taskUpdate := models.Task{}
	err = json.Unmarshal(body, &taskUpdate)
	if err != nil {
		fmt.Println(("UpdateTask: Marshalling failed"))
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	taskUpdate.Prepare()
	err = taskUpdate.Validate("update")
	if err != nil {
		fmt.Println(("UpdateTask: Task validation failed"))
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	taskUpdate.ID = uint(tid)
	taskUpdate.OwnerID = auth_uid //this is important to tell the model the task id to update, the other update field are set above

	//? This is followed from the tutorial but I wonder why can't we just call UpdateATask on the task var
	taskUpdated, err := taskUpdate.UpdateATask(a.DB)

	if err != nil {
		fmt.Println(("UpdateTask: Updating task failed"))
		//formattedError := formaterror.FormatError(err.Error())
		responses.ERROR(w, http.StatusInternalServerError, err)
		return
	}
	responses.JSON(w, http.StatusOK, taskUpdated)
}

func (a *App) DeleteTask(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	uid, err := strconv.ParseUint(vars["uid"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return

	}

	tid, err := strconv.ParseUint(vars["id"], 10, 64)
	if err != nil {
		responses.ERROR(w, http.StatusUnprocessableEntity, err)
		return
	}

	//check if we an authorized token id?
	//TODO: Do we need to do this if the authentication middldleware is working?
	auth_uid, err := auth.ExtractTokenID(r)
	if err != nil {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}

	//Check if the uid for the token matches the uid for the request
	if uint32(uid) != auth_uid {
		responses.ERROR(w, http.StatusUnauthorized, errors.New(http.StatusText(http.StatusUnauthorized)))
		return
	}

	// Check if the task exist
	task := models.Task{}
	taskReceived, err := task.FindTaskByID(a.DB, tid)
	if err != nil {
		responses.ERROR(w, http.StatusNotFound, err)
		return
	}

	//Is the authenticated user, the owner of this task?
	if auth_uid != taskReceived.OwnerID {
		responses.ERROR(w, http.StatusUnauthorized, errors.New("Unauthorized"))
		return
	}
	_, err = taskReceived.DeleteATask(a.DB, tid, auth_uid)
	if err != nil {
		responses.ERROR(w, http.StatusBadRequest, err)
		return
	}
	w.Header().Set("Entity", fmt.Sprintf("%d", tid))
	responses.JSON(w, http.StatusNoContent, "")
}
