package models

import (
	"errors"
	"html"
	"strings"

	"github.com/badoux/checkmail"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model

	//TODO: Readup on gorm tags
	Email    string `gorm:"size:100;not null;unique" json:"email"`
	Nickname string `gorm:"size:255;not null;unique" json:"nickname"`
	Password string `gorm:"size:100;not null;" json:"password"`
}

func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

func VerifyPassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func (u *User) BeforeSave(tx *gorm.DB) error {

	hashedPassword, err := Hash(u.Password)
	if err != nil {
		return err
	}
	u.Password = string(hashedPassword)
	return nil
}

func (u *User) Prepare() {

	u.Nickname = html.EscapeString(strings.TrimSpace(u.Nickname))
	u.Email = html.EscapeString(strings.TrimSpace(u.Email))
}

func (u *User) Validate(action string) error {
	switch strings.ToLower(action) {

	case "login":
		if u.Nickname == "" {
			return errors.New("Required Nickname")
		}
		if u.Password == "" {
			return errors.New("Required Password")
		}
		if u.Email == "" {
			return errors.New("Required Email")
		}
		if err := checkmail.ValidateFormat(u.Email); err != nil {
			return errors.New("Invalid Email")
		}
		return nil

	default:
		if u.Nickname == "" {
			return errors.New("Required Nickname")
		}
		if u.Password == "" {
			return errors.New("Required Password")
		}
		if u.Email == "" {
			return errors.New("Required Email")
		}
		if err := checkmail.ValidateFormat(u.Email); err != nil {
			return errors.New("Invalid Email")
		}
		return nil
	}
}

// SaveUser adds user to the database
func (u *User) SaveUser(db *gorm.DB) (*User, error) {

	// Debug a single operation, show detailed log for this operation
	err := db.Create(&u).Error
	if err != nil {
		return &User{}, err //? Why do we return an empty user?
	}
	return u, nil
}

// GetUser returns based on email
func (u *User) GetUser(db *gorm.DB) (*User, error) {

	account := &User{}
	if err := db.Table("users").Where("email = ?", u.Email).First(account).Error; err != nil {
		return nil, err
	}
	return account, nil
}

func (u *User) FindUserByID(db *gorm.DB, pid uint64) (*User, error) {

	if err := db.Model(&User{}).Where("id = ?", pid).Take(&u).Error; err != nil {
		return nil, err
	}

	return u, nil
}

// GetAllUsers returns a list all the user
func GetAllUsers(db *gorm.DB) (*[]User, error) {

	users := []User{}
	//? Does using Find without conditions return all users? Diff of Where vs Find with conditions?
	if err := db.Table("users").Find(&users).Error; err != nil {
		return &[]User{}, err
	}
	return &users, nil
}

func (u *User) DeleteAUser(db *gorm.DB, uid uint32) (int64, error) {

	db = db.Unscoped().Model(&User{}).Where("id = ?", uid).Take(&User{}).Delete(&User{})

	if db.Error != nil {
		return 0, db.Error
	}
	return db.RowsAffected, nil
}
