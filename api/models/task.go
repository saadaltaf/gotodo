package models

import (
	"errors"
	"fmt"
	"html"
	"strings"

	"gorm.io/gorm"
)

type Task struct {
	gorm.Model

	Title       string `gorm:"size:100;not null"     json:"title"` //TODO: How can we make title unique for a user, not for the entire table
	Description string `gorm:"size:500;not null"     json:"desc"`
	//DueDate     time.Time `json:"due_date"` //TODO: validation if it's in past?
	Owner   User   `gorm:"foreignKey:OwnerID;"       json:"-"`
	OwnerID uint32 `gorm:"not null"                  json:"owner_id"`
}

func (t *Task) Prepare() {

	t.Title = html.EscapeString(strings.TrimSpace(t.Title))
	t.Description = html.EscapeString(strings.TrimSpace(t.Description))
	t.Owner = User{}
}

func (t *Task) Validate(action string) error {

	switch strings.ToLower(action) {

	case "update":
		return nil

	default:
		if t.Title == "" {
			return errors.New("Required Title")
		}
		return nil
	}
}

func (t *Task) SaveTask(db *gorm.DB) (*Task, error) {

	fmt.Println("\nSaving Task")
	err := db.Model(&Task{}).Create(&t).Error
	if err != nil {
		fmt.Println(err)
		return &Task{}, err
	}

	fmt.Println("\nVerifying Save.....")
	if t.ID != 0 {
		err = db.Model(&User{}).Where("id = ?", t.OwnerID).Take(&t.Owner).Error
		if err != nil {
			return &Task{}, err
		}
	}
	return t, nil
}

func (t *Task) FindTaskByID(db *gorm.DB, tid uint64) (*Task, error) {

	err := db.Model(&Task{}).Where("id = ?", tid).Take(&t).Error
	if err != nil {
		return &Task{}, err
	}
	if t.ID != 0 {
		err = db.Model(&User{}).Where("id = ?", t.OwnerID).Take(&t.Owner).Error
		if err != nil {
			return &Task{}, err
		}
	}
	return t, nil
}

func (t *Task) UpdateATask(db *gorm.DB) (*Task, error) {

	err := db.Model(&Task{}).Where("id = ?", t.ID).Updates(Task{Title: t.Title, Description: t.Description}).Error
	if err != nil {
		return &Task{}, err
	}
	if t.ID != 0 {
		err = db.Model(&User{}).Where("id = ?", t.OwnerID).Take(&t.Owner).Error
		if err != nil {
			fmt.Println("Just couldn't get the owner ", t.OwnerID)
			return &Task{}, err
		}
	}
	return t, nil
}

func (t *Task) DeleteATask(db *gorm.DB, tid uint64, uid uint32) (int64, error) {

	db = db.Model(&Task{}).Where("id = ? and owner_id = ?", tid, uid).Take(&Task{}).Delete(&Task{})

	if db.Error != nil {
		if errors.Is(db.Error, gorm.ErrRecordNotFound) {
			return 0, errors.New("Task not found")
		}
		return 0, db.Error
	}
	return db.RowsAffected, nil
}

func GetAllTasksForUser(db *gorm.DB, u_id uint32) (*[]Task, error) {

	tasks := []Task{}

	if err := db.Table("tasks").Where("owner_id = ?", u_id).Find(&tasks).Error; err != nil {
		return &[]Task{}, err
	}
	return &tasks, nil
}
