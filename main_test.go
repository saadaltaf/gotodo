//TODO: Need to find out if all tests in this app should be definied in main

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab/saadaltaf/gotodo/api/controllers"
	"gitlab/saadaltaf/gotodo/api/models"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	"github.com/joho/godotenv"
)

var a controllers.App

func TestMain(m *testing.M) {
	if err := godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}

	//TODO: Do we need a different DB to run tests? Figure out how later
	a = controllers.App{}
	a.Initialize(
		os.Getenv("DB_HOST"),
		os.Getenv("DB_PORT"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_NAME"),
		os.Getenv("DB_PASSWORD"))

	fmt.Println("About to Run Server")
	a.RunServer()

	fmt.Println("Ensuring Tables Exist")
	ensureTablesExists()

	code := m.Run()
	clearTables()

	os.Exit(code)
}

func ensureTablesExists() {

	if !a.DB.Migrator().HasTable("users") {
		log.Fatal("Error: User Table does not exist")
	}

	if !a.DB.Migrator().HasTable("tasks") {
		log.Fatal("Error: Tasks Table does not exist")
	}
}

func clearTables() {

	a.DB.Exec("DELETE FROM users")
	a.DB.Exec("ALTER SEQUENCE user_id_seq RESTART WITH 1")

	a.DB.Exec("DELETE FROM tasks")
	a.DB.Exec("ALTER SEQUENCE task_id_seq RESTART WITH 1")
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func createUserReq(nname string, email string) *http.Request {
	var jsonStr = []byte(fmt.Sprintf(`{"nickname":"%v", "email": "%v"}`, nname, email))
	req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	return req
}

func createTaskReq(title string, desc string, ddate time.Time, u_id int32) *http.Request {
	var jsonStr = []byte(fmt.Sprintf(`{"title":"%v", "desc": "%v"}`, title, desc))

	//fmt.Println(jsonStr)
	req, _ := http.NewRequest("POST", fmt.Sprintf(`"users/%v/tasks"`, u_id), bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	fmt.Print(req)

	return req
}

func addTasks(count int, u *models.User) {
	if count < 1 {
		count = 1
	}

	for i := 0; i < count; i++ {

		title := fmt.Sprintf("Test Todo Title #%v", i)
		desc := fmt.Sprintf("I have to do this tasks in %v hours!!", i*2+1)

		//dur := time.Duration(i*2+1) * time.Hour
		//ddate := time.Now().Add(dur)

		t := &models.Task{Title: title, Description: desc, Owner: *u, OwnerID: uint32(u.ID)}

		t.SaveTask(a.DB)

	}
}

// *Tests

func TestCreateUser(t *testing.T) {

	//TODO: Does clearing table takes time / is async? Because user creation fails for subsequent tests if I use the same nickname or email for them
	clearTables()

	req := createUserReq("user1", "user1@tintash.com")
	response := executeRequest(req)
	checkResponseCode(t, http.StatusCreated, response.Code)

	u := &models.User{}

	err := json.Unmarshal(response.Body.Bytes(), u)

	if err != nil {
		t.Errorf("Could not compose a valid User model from the response %v", response.Body)
	} else {

		if u.Nickname != "user1" {
			t.Errorf("Expected user nickname to be 'user1'. Got '%v'", u.Nickname)
		}

		if u.Email != "user1@tintash.com" {
			t.Errorf("Expected user email to be 'user1@tintash.com'. Got '%v'", u.Email)
		}
	}

}

func TestCreateTaskForUser(t *testing.T) {
	clearTables()

	req := createUserReq("user2", "user2@tintash.com")
	response := executeRequest(req)

	u := models.User{}
	if err := a.DB.Debug().Table("users").Where("email = ?", "user2@tintash.com").First(&u).Error; err != nil {
		t.Errorf("Could not create user, test user creation first")
	}

	title := "Saad's Test Todo"
	desc := "This is a test description, it could be long or short. For now its short"
	ddate := time.Now().Add(time.Hour)

	req = createTaskReq(title, desc, ddate, int32(u.ID))
	response = executeRequest(req)

	checkResponseCode(t, http.StatusCreated, response.Code)
}

func TestGetTasksForUser(t *testing.T) {
	clearTables()

	req := createUserReq("user3", "user3@tintash.com")
	response := executeRequest(req)

	u := models.User{}
	if err := a.DB.Debug().Table("users").Where("email = ?", "user3@tintash.com").First(&u).Error; err != nil {
		t.Errorf("Could not create user, test user creation first")
	}

	fmt.Print("Printing user\n\n\n", u.Email)
	//Add some dummy tasks to db directly against this user
	addTasks(3, &u)

	jsonBody := []byte(fmt.Sprintf(``))
	bodyReader := bytes.NewReader(jsonBody)

	req, _ = http.NewRequest("GET", fmt.Sprintf(`"users/%v/tasks"`, u.ID), bodyReader)
	response = executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)
}
